﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using AutoPYP.Extensions;
using AutoPYP.Helper;
using AutoPYP.ViewModels.Base;
using Newtonsoft.Json;

namespace AutoPYP.Models
{
    public class ProjectDirectoryItem : INotifyPropertyChanged
    { 
        private const string OriginalFileExt = ".pyp";

        private FileSystemWatcher _watcher;
        public string FileName { get; set; }
        public string FilePath { get; set; }
        private string _fullPath;
        public IEnumerable<string> Children { get; set; }


        public bool IsPyp => FullPath != null && (Path.GetExtension(FullPath).Equals(OriginalFileExt, StringComparison.OrdinalIgnoreCase) || Path.GetExtension(FullPath).Equals(".off"));
        
        internal void Initialize()
        {
            _initialized = true;
        }

        public string FullPath
        {
            get => _fullPath;
            set
            {
                _fullPath = value;
                CheckIfValidFile();

                if(_watcher == null)
                {
                    Watch();
                }

            }
        }
        
        public bool AllowedToPackage
        {
            get => _allowedToPackage;
            set
            {
                _allowedToPackage = value;
                OnUpdated();
            }
        }

        private void OnUpdated()
        {
            OnAllowedToPackageChanged?.Invoke(Children);
        }

        [JsonIgnore] public string FileSize { get; set; }
        [JsonIgnore] public ICommand OpenLocationCommand { get; set; }
        [JsonIgnore] public ICommand DeleteFileCommand { get; set; }

        [JsonIgnore]
        public bool IsEnabled { get; set; }

        private void UpdateFileName()
        {
            if (!_initialized) return;

            if (!IsItemEnabled)
            {
                if (!string.IsNullOrEmpty(FullPath))
                {
                    var newName = Path.ChangeExtension(FullPath, OriginalFileExt);
                    DirectoryHelpers.SafeRenameFile(FullPath, newName);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(FullPath))
                {
                    var newName = Path.ChangeExtension(FullPath, "off");
                    DirectoryHelpers.SafeRenameFile(FullPath, newName);
                }
            }
        }

        [JsonIgnore] public bool IsDirectory { get; set; }

        [JsonIgnore]
        public bool IsItemEnabled
        {
            get => FullPath != null && Path.GetExtension(FullPath).Equals(OriginalFileExt);
            set
            {
                UpdateFileName();
            }
        }

        [JsonIgnore]
        public bool IsValidFile { get; set; } = true;

        [JsonIgnore] public Action<IEnumerable<string>> OnAllowedToPackageChanged { get; set; }
        [JsonIgnore] public Action OnDirectoryUpdate { get; set; }
        public string OffVariant => FullPath?.OffVariant();

        private readonly string[] _allowedExtention = { ".json", ".png", ".txt", ".fcs" };
        private bool _allowedToPackage;
        private bool _initialized;

        private void CheckIfValidFile()
        {
            if (FullPath == null) return;
            IsValidFile = _allowedExtention.Contains(Path.GetExtension(FullPath));
        }

        public ProjectDirectoryItem()
        {
            OpenLocationCommand = new RelayCommand(OpenLocationMethod);
            DeleteFileCommand = new RelayCommand(DeleteFileCommandMethod);
        }

        private void DeleteFileCommandMethod()
        {
            DirectoryHelpers.SafeFileDelete(FullPath);
        }

        private void OpenLocationMethod()
        {
            try
            {
                var args = $"/e, /select, \"{FullPath}\"";
                var info = new ProcessStartInfo { FileName = "explorer", Arguments = args };
                Process.Start(info);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public override string ToString()
        {
            return $"File Name : {FileName}";
        }

        private void Watch()
        {
            if (FullPath == null || !DirectoryHelpers.IsDirectory(FullPath)) return;

            _watcher = new FileSystemWatcher();
            _watcher.Path = FullPath;
            _watcher.NotifyFilter = NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.FileName
                                    | NotifyFilters.Size
                                    | NotifyFilters.DirectoryName;
            _watcher.Filter = "*.*";
            _watcher.Changed += OnChanged;
            _watcher.Created += OnChanged;
            _watcher.Deleted += OnChanged;
            _watcher.Renamed += OnChanged;
            _watcher.EnableRaisingEvents = true;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            App.Current.Dispatcher?.Invoke(Trigger);
        }

        private void Trigger()
        {
            OnDirectoryUpdate?.Invoke();
        }

        public void UnSubscribe()
        {
            if (_watcher == null) return;

            _watcher.Changed -= OnChanged;
            _watcher.Created -= OnChanged;
            _watcher.Deleted -= OnChanged;
            _watcher.Renamed -= OnChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

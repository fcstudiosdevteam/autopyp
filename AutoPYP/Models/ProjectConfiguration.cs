﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoPYP.Models
{
    public class ProjectConfiguration
    {
        public ProjectConfiguration()
        {
            
        }

        public ProjectConfiguration(int modId, List<ProjectDirectoryItem> files)
        {
            ModId = modId;
            DirectoryFiles = files;
        }
        public int ModId { get; set; }
        public List<ProjectDirectoryItem> DirectoryFiles { get; set; }
    }
}

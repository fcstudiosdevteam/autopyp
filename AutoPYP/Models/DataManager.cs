﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AutoPYP.Models.UIElements;
using AutoPYP.ViewModels.Pages;
using AutoPYP.Views;
using MRULib;

namespace AutoPYP.Models
{
    internal static class DataManager
    {
        public static ObservableCollection<CustomTab> Tabs { get; set; } = new ObservableCollection<CustomTab>();
        public static CustomTab SelectedTab { get; set; }
        public const string NewDocumentName = "New Project";

        public static void CloseTab(CustomTab tabItem)
        {
            bool tryCloseResult = true;


            if (tabItem.IsEdited())
            {
                tryCloseResult = TryClose(null);
            }

            if (tryCloseResult)
            {
                CloseTabOperation(tabItem);
            }
        }

        private static void CloseTabOperation(CustomTab tabItem)
        {
            //Remove tab
            Tabs.Remove(tabItem);
        }

        public static void OpenNewTab(ProjectPageContentViewModel vm = null)
        {
            bool fromSave = false;
            if (vm != null)
            {
                //See if tab is open.
                var match = Tabs.SingleOrDefault(x => x.ProjectItem.SaveLocation.Equals(vm.SaveLocation));

                if (match != null)
                {
                    match.IsSelected = true;
                    return;
                }

                fromSave = true;
            }
            else
            {
                vm = new ProjectPageContentViewModel();
            }
            
            //Create a new mod page
            var content = new ProjectPageContentView();

            int newProjectCount = 0;
            foreach (CustomTab customTab in Tabs)
            {
                if (customTab.Header.ToString().Trim().StartsWith(NewDocumentName))
                {
                    newProjectCount += 1;
                }
            }

            var projectCountAmount = newProjectCount > 0 ? newProjectCount.ToString() : string.Empty;
            
            var tab = new CustomTab
            {
                Header = fromSave ? Path.GetFileNameWithoutExtension(vm.SaveLocation) : $"{NewDocumentName} {projectCountAmount}",
                ProjectItem = vm,
                IsSelected = true,
                Content = content
            };

            vm.Tab = tab;
            content.DataContext = vm;
            
            //Add new tab
            Tabs.Add(tab);
        }
        
        public static bool TryClose(Action method)
        {
            var result =
                MessageBox.Show(
                    "Changes where made! Would you like to save changes before closing?",
                    "Mod Not Saved", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation);

            switch (result)
            {
                case MessageBoxResult.Cancel:
                    return false;
                case MessageBoxResult.Yes:
                    method?.Invoke();
                    break;
            }

            return true;
        }

        public static void OpenNewProjectPage()
        {
            var headerName = "New Project";

            //See if tab is open.
            var match = Tabs.SingleOrDefault(x => x.Header.Equals(headerName));

            if (match != null)
            {
                match.IsSelected = true;
                return;
            }

            //Create a new mod page
            var page = new NewProjectContentView();

            var tab = new CustomTab
            {
                Header = headerName,
                IsSelected = true,
                Content = page
            };

            //Add new tab
            Tabs.Add(tab);
        }

        public static bool IsAnyEdited()
        {
            var isEdited = Tabs.SingleOrDefault(x => x.IsEdited());
            return isEdited != null;
        }
    }
}

﻿using System;
using System.IO;
using System.Threading.Tasks;
using MRULib;
using MRULib.MRU.Interfaces;
using MRULib.MRU.Models.Persist;

namespace AutoPYP.Models
{
    public class MRUList
    {
        private IMRUListViewModel _MRUFilelist;

        public MRUList()
        {
            if (File.Exists(App.MRUSaveLocation))
            {
                _MRUFilelist = MRUEntrySerializer.Load(App.MRUSaveLocation);
                _MRUFilelist.UpdateEntry(MRU_Service.Create_Entry("Clear All", true));
            }
        }

        private async Task LoadData()
        {
            SetMRUList(await MRUEntrySerializer.LoadAsync(App.MRUSaveLocation));
        }

        public IMRUListViewModel MRUFileList
        {
            get
            {
                return _MRUFilelist;
            }
        }

        private void SetMRUList(IMRUListViewModel mruFilelist)
        {
            if (mruFilelist == null)
                return;

            this._MRUFilelist = mruFilelist;
        }
    }
    internal static class GenerateTestData
    {
        internal static IMRUListViewModel CreateTestData()
        {
            var retList = MRU_Service.Create_List();

            var now = DateTime.Now;

            retList.UpdateEntry(MRU_Service.Create_Entry(@"C:tmp\t0_now.txt", false));

            // This should be shown yesterday if update and grouping work correctly
            retList.UpdateEntry(MRU_Service.Create_Entry(@"C:tmp\t15_yesterday.txt", now.Add(new TimeSpan(-20, 0, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:\tmp\t15_yesterday.txt", now.Add(new TimeSpan(-1, 0, 0, 0)), false));

            retList.UpdateEntry(MRU_Service.Create_Entry(@"f:tmp\t1_today.txt", now.Add(new TimeSpan(-1, 0, 0, 0)), true));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"f:tmp\Readme.txt", now.Add(new TimeSpan(-1, 0, 0, 0)), true));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\t2_today.txt", now.Add(new TimeSpan(0, -1, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\t3_today.txt", now.Add(new TimeSpan(0, -10, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\t4_today.txt", now.Add(new TimeSpan(0, 0, -1, 0)), false));

            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\t5_today.txt", now.Add(new TimeSpan(0, -20, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\t5_today.txt", now.Add(new TimeSpan(0, -20, 0, 0)), false));

            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\t6_today.txt", now.Add(new TimeSpan(0, -44, 0, 0)), false));

            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t7_today.txt", now.Add(new TimeSpan(-30, 0, 0, 0)), true));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t8_today.txt", now.Add(new TimeSpan(-25, 0, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t9_today.txt", now.Add(new TimeSpan(-20, 0, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t10_today.txt", now.Add(new TimeSpan(-10, 0, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t11_today.txt", now.Add(new TimeSpan(-5, 0, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t12_today.txt", now.Add(new TimeSpan(-4, 0, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t13_today.txt", now.Add(new TimeSpan(-3, 0, 0, 0)), false));
            retList.UpdateEntry(MRU_Service.Create_Entry(@"c:tmp\directory1\directory2\directory3\t14_today.txt", now.Add(new TimeSpan(-2, 0, 0, 0)), false));

            return retList;
        }
    }
}

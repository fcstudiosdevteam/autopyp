﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using AutoPYP.ViewModels.Pages;

namespace AutoPYP.Models.UIElements
{
    public class CustomTab : TabItem
    {
        /// <summary>
        /// Boolean to state if the tab has content has been edited
        /// </summary>
        private bool _contentEdited;

        public ProjectPageContentViewModel ProjectItem { get; set; }

        public string GetRawName()
        {
            var header = Header.ToString();

            var result = header.Substring(header.Length - 1);

            if (result.Equals("*"))
            {
                header = header.Remove(header.Length - 1, 1);
            }

            return header;
        }

        public void UpdateLabel(bool value = true)
        {
            var header = Header.ToString();

            var result = header.Substring(header.Length - 1);

            if(!value && result.Equals("*"))
            {
                Header = header.Remove(header.Length - 1, 1);
                _contentEdited = false;
                return;
            }

            if (!result.Equals("*") && value)
            {
                Header = $"{Header}*";
                _contentEdited = true;
            }
        }

        public bool IsEdited()
        {
            return _contentEdited;
        }

        public void RefreshData(string argsFullPath)
        {

            Application.Current.Dispatcher.Invoke(new System.Action(() =>
            {
                var header = Header.ToString();

                var result = header.Substring(header.Length - 1);

                if (result.Equals("*"))
                {
                    Header = $"{Path.GetFileName(argsFullPath)}*";
                    _contentEdited = true;
                }
                else
                {
                    Header = Path.GetFileName(argsFullPath);
                }
            }));

            
        }

        public void ChangeHeaderName (string newName)
        {
            Header = newName;
            UpdateLabel(_contentEdited);
        }
    }
}

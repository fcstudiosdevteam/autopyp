﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Windows;
using AutoPYP.ViewModels.Base;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace AutoPYP.Models
{
    public class QuickLogger : BaseViewModel
    {
        public ObservableCollection<MessageLog> Log { get; set; } = new ObservableCollection<MessageLog>();

        public Action OnMessgeLogUpdate { get; set; }
        
        internal void Info(string message)
        {
            if (Application.Current.Dispatcher != null)
            {
                Application.Current.Dispatcher.Invoke((Action)(() => Log.Add(new MessageLog{DateTime = DateTime.Now.ToString(), Message = message, Type = MessageType.Info})));
            }

            OnMessgeLogUpdate?.Invoke();
        }

        internal void Error(string message)
        {
            if (Application.Current.Dispatcher != null)
            {
                Application.Current.Dispatcher.Invoke((Action)(() => Log.Add(new MessageLog { DateTime = DateTime.Now.ToString(), Message = message, Type = MessageType.Error})));
            }

            OnMessgeLogUpdate?.Invoke();
        }

        internal void ExportLog()
        {
            try
            {
                using (CommonSaveFileDialog sfd = new CommonSaveFileDialog())
                {
                    string str = DateTime.Now.ToString("dd_M_yyyy_HH_mm_ss", CultureInfo.InvariantCulture);

                    sfd.AlwaysAppendDefaultExtension = true;
                    sfd.DefaultExtension = "txt";
                    sfd.DefaultFileName = $"AutoPYPLog_{str}";
                    sfd.Filters.Add(new CommonFileDialogFilter("Text File", "*.txt"));

                    if (sfd.ShowDialog() == CommonFileDialogResult.Ok)
                    {
                        var sb = new StringBuilder();

                        foreach (MessageLog messageLog in Log)
                        {
                            sb.Append($"[{messageLog.DateTime}] [{messageLog.Type}]: {messageLog.Message}");
                            sb.Append(Environment.NewLine);
                        }


                        File.WriteAllText(sfd.FileName, sb.ToString());
                        Info($"Exported Log to: {sfd.FileName}");
                    }
                }
            }
            catch (Exception ex)
            {
                Error(ex.Message);
            }
        }

        public void Clear()
        {
           Log.Clear();
            Action onMessgeLogUpdate = OnMessgeLogUpdate;
            onMessgeLogUpdate?.Invoke();
        }
    }

    public class MessageLog
    {
        public string Message { get; set; }
        public string DateTime { get; set; }
        public MessageType Type { get; set; }
    }
}

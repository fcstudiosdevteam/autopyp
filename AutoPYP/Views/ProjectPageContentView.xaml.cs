﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using AutoPYP.Models;
using AutoPYP.Models.UIElements;

namespace AutoPYP.Views
{
    /// <summary>
    /// Interaction logic for ProjectPageContentView.xaml
    /// </summary>
    public partial class ProjectPageContentView : UserControl
    {
        public ProjectPageContentView()
        {
            InitializeComponent();
        }

        public ObservableCollection<ProjectDirectoryItem> DirectoryFilesList
        {
            get => (ObservableCollection<ProjectDirectoryItem>)GetValue(DirectoryFilesListProperty);
            set => SetValue(DirectoryFilesListProperty, value);
        }
        
        // Using a DependencyProperty as the backing store for IsExpanded.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DirectoryFilesListProperty =
            DependencyProperty.Register(nameof(DirectoryFilesList), typeof(ObservableCollection<ProjectDirectoryItem>), typeof(ProjectPageContentView), new PropertyMetadata());

    }
}

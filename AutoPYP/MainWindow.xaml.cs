﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AutoPYP.Models;
using AutoPYP.Models.UIElements;
using AutoPYP.ViewModels.Pages;
using AutoPYP.Views;
using Microsoft.WindowsAPICodePack.Dialogs;
using Newtonsoft.Json;

namespace AutoPYP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Close_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void New_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataManager.OpenNewTab();
        }

        private void Save_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataManager.SelectedTab?.ProjectItem.SaveData();
        }

        private void SaveAll_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.Save();

            var isEdited = DataManager.IsAnyEdited();

            if (isEdited)
            {

                var message =
                    MessageBox.Show(
                        "Changes were made and are not saved are you sure you would like to close without saving?",
                        "Changes where made!", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

                switch (message)
                {
                    case MessageBoxResult.No:
                        e.Cancel = true;
                        break;
                }
            }

            base.OnClosing(e);
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            App.Initialized = true;
            App.StayOnTopCommandMethod(Properties.Settings.Default.StayOnTop, this);
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0 && e.AddedItems[0] is CustomTab)
            {
                DataManager.SelectedTab = (CustomTab)e.AddedItems[0];
            }
        }

        private void Open_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            using (CommonOpenFileDialog ofd = new CommonOpenFileDialog())
            {
                ofd.Filters.Add(new CommonFileDialogFilter("AutoPYP Project", "*.apyp"));

                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    var settings = new JsonSerializerSettings
                    {
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };

                    var jsonToString = File.ReadAllText(ofd.FileName);
                    var vm = JsonConvert.DeserializeObject<ProjectPageContentViewModel>(jsonToString, settings);

                    vm.SaveLocation = ofd.FileName;
                    DataManager.OpenNewTab(vm);
                }
            }
        }

        private void About_OnClick(object sender, RoutedEventArgs e)
        {
            var about = new AboutWindowView();
            about.ShowDialog();
        }

        private void Help_OnClick(object sender, RoutedEventArgs e)
        {

        }

        private void LoadFile_CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var settings = new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                var file = e.Parameter.ToString();

                if (file.Equals("Clear All"))
                {
                    App.MruList.MRUFileList.Clear();
                    return;
                }
                
                var jsonToString = File.ReadAllText(file);
                var vm = JsonConvert.DeserializeObject<ProjectPageContentViewModel>(jsonToString, settings);
                vm.SaveLocation = file;
                DataManager.OpenNewTab(vm);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExportLog_OnClick(object sender, RoutedEventArgs e)
        {
            DataManager.SelectedTab?.ProjectItem.QuickLogger.ExportLog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using AutoPYP.Models;
using AutoPYP.ViewModels.Pages;
using Newtonsoft.Json;

namespace AutoPYP.Helper
{
    public static class DirectoryHelpers
    {
        private static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        
        public static string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }
        
        public static string GetFileSize(string path)
        {
            string result = "N/A";
            try
            {
                if (!File.Exists(path)) return result;
                FileInfo fi = new FileInfo(path);
                // Get file size  
                long size = fi.Length;
                result = SizeSuffix(size);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return result;
            }

            return result;
        }

        public static bool IsDirectory(string path)
        {
            try
            {
                if (!File.Exists(path) && !Directory.Exists(path)) return false;
                // get the file attributes for file or directory
                FileAttributes attr = File.GetAttributes(path);

                return attr.HasFlag(FileAttributes.Directory);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        public static string GetDirectorySize(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            var dirSize = di.EnumerateFiles("*", SearchOption.AllDirectories).Sum(fi => fi.Length);
            return SizeSuffix(dirSize);
        }

        public static void SafeFileDelete(string path, bool verbose = true)
        {
            if (verbose)
            {
                var result = MessageBox.Show($"Are you sure you would like to delete {Path.GetFileName(path)}. This operation cannot be undone!", "Delete File.", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    DeleteFile(path);
                }
            }
            else
            {
                DeleteFile(path);
            }
        }

        private static void DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static ProjectPageContentViewModel LoadConfigurationData(string path)
        {
            try
            {
                // == Load Configuration == //
                string configJson = File.ReadAllText(path.Trim());

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.MissingMemberHandling = MissingMemberHandling.Ignore;

                // == LoadData == //
                return JsonConvert.DeserializeObject<ProjectPageContentViewModel>(configJson, settings);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public static void SaveConfigurationData(string path, ProjectConfiguration data)
        {
            try
            {
                var saveDataJson = JsonConvert.SerializeObject(data, Formatting.Indented);

                File.WriteAllText(path.Trim(), saveDataJson);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void Copy(string sourceDirectory, string targetDirectory)
        {
            var diSource = new DirectoryInfo(sourceDirectory);
            var diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        public static void SafeDeleteDirectory(string targetDirectory, bool recursive = true)
        {
            try
            {
                Directory.Delete(targetDirectory, recursive);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static void SafeRenameFile(string fullPath, string newFullPath)
        {
            try
            {
                File.Move(fullPath, newFullPath);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public static IEnumerable<string> GetChildren(string path)
        {

            try
            {
                if (!IsDirectory(path)) return null;

                var dir = Directory.GetDirectories(path, "*", SearchOption.AllDirectories);

                var files = Directory.GetFiles(path, "*", SearchOption.AllDirectories);

                int array1OriginalLength = files.Length;
                Array.Resize<string>(ref files, array1OriginalLength + dir.Length);
                Array.Copy(dir, 0, files, array1OriginalLength, dir.Length);

                return files;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }
    }
}
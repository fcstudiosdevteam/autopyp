﻿using System.Windows.Input;

namespace AutoPYP.CommandBindings
{
    public class AppCommands
    {
        static AppCommands()
        {
            SaveAll = new RoutedUICommand(
                "S_aveAll", "Save All", typeof(AppCommands),
                new InputGestureCollection{ new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+S")});

            LoadFile = new RoutedUICommand(Strings.CMD_APP_Open_Description, "LoadFile", typeof(AppCommands), new InputGestureCollection());
        }
        public static RoutedUICommand SaveAll { get; }
        public static RoutedUICommand LoadFile { get; }
    }
}

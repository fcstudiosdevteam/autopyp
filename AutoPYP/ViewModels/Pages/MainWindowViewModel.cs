﻿using System.Collections.ObjectModel;
using System.Windows;
using AutoPYP.Models;
using AutoPYP.Models.UIElements;
using AutoPYP.ViewModels.Base;

namespace AutoPYP.ViewModels.Pages
{
    public class MainWindowViewModel : BaseViewModel
    {
        private CustomTab _selectedTab;

        private bool _stayOnTop;

        public MainWindowViewModel()
        {
            StayOnTop = Properties.Settings.Default.StayOnTop;
            RecentFiles = App.MruList;
        }

        public bool StayOnTop
        {
            get => _stayOnTop;
            set
            {
                _stayOnTop = value;
                if (App.Initialized)
                {
                    App.StayOnTopCommandMethod(value);
                    Properties.Settings.Default.StayOnTop = value;
                }
            }
        }

        public CustomTab SelectedTab
        {
            get => _selectedTab;
            set
            {
                _selectedTab = value;
                DataManager.SelectedTab = value;
            }
        }
        
        public MRUList RecentFiles { get; }

        public string ApplicationName => $"AutoPYP - {App.Version}";
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using AutoPYP.Enums;
using AutoPYP.Extensions;
using AutoPYP.Helper;
using AutoPYP.Models;
using AutoPYP.Models.UIElements;
using AutoPYP.ViewModels.Base;
using Ionic.Zip;
using Microsoft.WindowsAPICodePack.Dialogs;
using MRULib;
using MRULib.MRU.Models.Persist;
using Newtonsoft.Json;

namespace AutoPYP.ViewModels.Pages
{
    public class ProjectPageContentViewModel : BaseViewModel
    {
        private string _workSpacePath;
        private string _symbolsCache = "symbolcache";
        private string _uniqueId;
        private bool _deletePyc;
        private bool _deleteOtherFiles;
        private bool _deleteSymbolsCache;
        private string _outputBrowsePath;
        private FileSystemWatcher _watcher;
        private ObservableCollection<ProjectDirectoryItem> _tempCollection;

        [JsonIgnore] public ICommand ExecuteClickCommand { get; set; }
        [JsonIgnore] public ICommand ClearLogClickCommand { get; set; }
        [JsonIgnore] public ICommand OutputBrowseClickCommand { get; set; }
        [JsonIgnore] public ICommand WorkSpaceBrowseClickCommand { get; set; }
        [JsonIgnore] public ICommand DeleteFileBrowseClickCommand { get; set; }
        [JsonIgnore] public ICommand PYCDirectoryListBrowseClickCommand { get; set; }
        [JsonIgnore] public ICommand PublishClickCommand { get; set; }
        [JsonIgnore] public ICommand DeletePYEntryCommand { get; set; }
        [JsonIgnore] public ICommand DeleteFileEntryCommand { get; set; }
        [JsonIgnore] public ICommand DisablePYPClickCommand { get; set; }
        [JsonIgnore] public ICommand EnablePYPClickCommand { get; set; }
        [JsonIgnore] public QuickLogger QuickLogger { get; set; } = new QuickLogger();
        [JsonIgnore] public string SaveLocation { get; set; }
        public ObservableCollection<ProjectDirectoryItem> PYDirectoryFilesList { get; set; } = new ObservableCollection<ProjectDirectoryItem>();
        public ObservableCollection<ProjectDirectoryItem> DeleteFilesList { get; set; } = new ObservableCollection<ProjectDirectoryItem>();
        public ObservableCollection<ProjectDirectoryItem> ProjectFilesList { get; set; } = new ObservableCollection<ProjectDirectoryItem>();
        public ObservableCollection<ProjectDirectoryItem> DirectoryItemsCollection { get; set; } = new ObservableCollection<ProjectDirectoryItem>();

        public string WorkSpacePath
        {
            get => _workSpacePath;
            set
            {
                _workSpacePath = value;
                UpdateTabName();
                OnEditedChanged();
                RefreshDirectoryList();
                Watch();
            }
        }

        private void RefreshDirectoryList()
        {
            foreach (ProjectDirectoryItem modDirectoryFile in DirectoryItemsCollection)
            {
                modDirectoryFile.OnAllowedToPackageChanged -= OnEditedChanged;
                modDirectoryFile.OnDirectoryUpdate -= OnDirectoryUpdate;
                modDirectoryFile.UnSubscribe();
            }

            var files = DirectoryHelpers.GetChildren(WorkSpacePath);



            if (DirectoryItemsCollection.Count > 0)
            {
                _tempCollection = new ObservableCollection<ProjectDirectoryItem>(DirectoryItemsCollection);
            }

            DirectoryItemsCollection.Clear();

            foreach (string file in files)
            {
                if(Path.GetFileName(file).Equals(".git") || Path.GetFileName(file).Equals(".vscode") || Path.GetFileName(file).Equals(".gitignore")) continue; //TODO Add ignore name list

                ProjectDirectoryItem projectItem = null;
                
                var isSaveFile = Path.GetExtension(file).Equals(".apyp");
                var isDirectory = DirectoryHelpers.IsDirectory(file);
                ProjectDirectoryItem match = null;
                
                if (_tempCollection != null)
                {
                    match = _tempCollection.FirstOrDefault(x => x.FullPath != null && (x.FullPath.Trim().Equals(file) || x.OffVariant.Trim().Equals(file.OffVariant())));
                }
                
                if (match != null)
                {
                    projectItem = new ProjectDirectoryItem
                    {

                        AllowedToPackage = match.AllowedToPackage,
                        FullPath = file,
                        IsDirectory = isDirectory,
                        FileName = Path.GetFileName(file),
                        FilePath = Directory.GetParent(file).FullName,
                        Children = DirectoryHelpers.GetChildren(file),
                        FileSize = DirectoryHelpers.IsDirectory(file)
                            ? DirectoryHelpers.GetDirectorySize(file)
                            : DirectoryHelpers.GetFileSize(file),
                        IsEnabled = !isSaveFile
                    };
                }
                else
                {
                    projectItem = new ProjectDirectoryItem
                    {
                        AllowedToPackage = true,
                        IsDirectory = isDirectory,
                        FullPath = file,
                        Children = DirectoryHelpers.GetChildren(file),
                        FileName = Path.GetFileName(file),
                        FilePath = Directory.GetParent(file).FullName,
                        FileSize = DirectoryHelpers.IsDirectory(file)
                            ? DirectoryHelpers.GetDirectorySize(file)
                            : DirectoryHelpers.GetFileSize(file),
                        IsEnabled = !isSaveFile
                    };
                }

                projectItem.OnAllowedToPackageChanged += OnEditedChanged;
                
                if (projectItem.IsDirectory)
                {
                    projectItem.OnDirectoryUpdate += OnDirectoryUpdate;
                }

                projectItem.Initialize();

                DirectoryItemsCollection.Add(projectItem);
            }

            _tempCollection?.Clear();
        }

        private void OnDirectoryUpdate()
        {
            RefreshDirectoryList();
        }

        public string UniqueID
        {
            get => _uniqueId;
            set
            {
                _uniqueId = value;
                OnEditedChanged(null);
            }
        }

        public bool DeletePYC
        {
            get => _deletePyc;
            set
            {
                _deletePyc = value;
                OnEditedChanged();
            }
        }

        public bool DeleteOtherFiles
        {
            get => _deleteOtherFiles;
            set
            {
                _deleteOtherFiles = value;
                OnEditedChanged();
            }
        }

        public bool DeleteSymbolsCache
        {
            get => _deleteSymbolsCache;
            set
            {
                _deleteSymbolsCache = value;
                OnEditedChanged();
            }
        }

        public string OutputBrowsePath
        {
            get => _outputBrowsePath;
            set
            {
                _outputBrowsePath = value; 
                OnEditedChanged();
            }
        }
        
        [JsonIgnore] public CustomTab Tab { get; set; }

        public ProjectPageContentViewModel()
        {
            ExecuteClickCommand = new RelayCommand(ExecuteClickCommandMethod);
            OutputBrowseClickCommand = new RelayCommand(OutputBrowseCommandMethod);
            WorkSpaceBrowseClickCommand = new RelayCommand(WorkSpaceBrowseClickCommandMethod);
            DeleteFileBrowseClickCommand = new RelayCommand(DeleteFileBrowseClickCommandMethod);
            PYCDirectoryListBrowseClickCommand = new RelayCommand(PYCDirectoryListBrowseClickCommandMethod);
            DeletePYEntryCommand = new RelayParameterizedCommand(DeletePYEntryCommandMethod);
            DeleteFileEntryCommand = new RelayParameterizedCommand(DeleteFileEntryCommandMethod);
            ClearLogClickCommand = new RelayCommand(ClearLogClickCommandMethod);
            PublishClickCommand = new RelayCommand(PublishClickCommandMethod);
            DisablePYPClickCommand = new RelayCommand(ToggleOffPluginClickCommandMethod);
            EnablePYPClickCommand = new RelayCommand(ToggleOnPluginClickCommandMethod);
            //TogglePluginClickCommand = new RelayCommand(TogglePluginClickCommandMethod);
            UniqueID = "MasterProject";
        }

        private void ToggleOnPluginClickCommandMethod()
        {
            if(string.IsNullOrEmpty(OutputBrowsePath)) return;
            var files = Directory.GetFiles(OutputBrowsePath, "*", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                if (file != null && Path.GetExtension(file).Equals(".off"))
                {
                    var newFile = Path.ChangeExtension(file, "pyp");
                    DirectoryHelpers.SafeRenameFile(file,newFile);
                }
            }
        }

        private void ToggleOffPluginClickCommandMethod()
        {
            if (string.IsNullOrEmpty(OutputBrowsePath)) return;
            var files = Directory.GetFiles(OutputBrowsePath, "*", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                if (file != null && Path.GetExtension(file).Equals(".pyp"))
                {
                    var newFile = Path.ChangeExtension(file, "off");
                    DirectoryHelpers.SafeRenameFile(file, newFile);
                }
            }
        }

        public ToggleState ToggleState { get; set; }

        private void ClearLogClickCommandMethod()
        {
            var dialog = MessageBox.Show("Are your sure you would like to clear the log?", "Clear the log",
                MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (dialog == MessageBoxResult.Yes)
            {
                QuickLogger.Clear();
            }
        }

        private void DeleteFileEntryCommandMethod(object obj)
        {
            try
            {
                var dirFile = (ProjectDirectoryItem)obj;

                if (dirFile != null)
                {
                    DeleteFilesList.Remove(dirFile);
                }
            }
            catch (Exception e)
            {
                QuickLogger.Error(e.Message);
            }
        }

        private void OnEditedChanged(IEnumerable<string> list = null)
        {
            Tab?.UpdateLabel();
            if (list != null)
            {
                DisableChildren(list);
            }
        }

        private void DisableChildren(IEnumerable<string> list)
        {
            foreach (string item in list)
            {
                foreach (ProjectDirectoryItem directoryItem in DirectoryItemsCollection)
                {
                    if (directoryItem.FullPath.Equals(item))
                    {
                        if (directoryItem.AllowedToPackage)
                        {
                            directoryItem.AllowedToPackage = false;
                        }
                        break;
                    }
                }
            }
        }

        private void DeletePYEntryCommandMethod(object obj)
        {
            try
            {
                var dirFile = (ProjectDirectoryItem)obj;

                if (dirFile != null)
                {
                    PYDirectoryFilesList.Remove(dirFile);
                }
            }
            catch (Exception e)
            {
                QuickLogger.Error(e.Message);
            }
        }

        private void PYCDirectoryListBrowseClickCommandMethod()
        {
            using (CommonOpenFileDialog ofd = new CommonOpenFileDialog())
            {
                ofd.IsFolderPicker = true;
                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    var fileName = Path.GetFileName(ofd.FileName);
                    if (fileName != null && fileName.Trim().Equals(string.Empty))
                    {
                        fileName = ofd.FileName;
                    }
                    PYDirectoryFilesList.Add(new ProjectDirectoryItem { FileName = fileName, FilePath = ofd.FileName });
                    OnEditedChanged();
                }
            }
        }

        private void DeleteFileBrowseClickCommandMethod()
        {
            using (CommonOpenFileDialog ofd = new CommonOpenFileDialog())
            {
                ofd.IsFolderPicker = false;
                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    DeleteFilesList.Add(new ProjectDirectoryItem { FileName = Path.GetFileName(ofd.FileName), FilePath = ofd.FileName });
                    OnEditedChanged();
                }
            }
        }
        
        private void UpdateTabName()
        {
            Tab?.ChangeHeaderName(Path.GetFileName(WorkSpacePath));
        }

        private void OutputBrowseCommandMethod()
        {
            using (CommonOpenFileDialog ofd = new CommonOpenFileDialog())
            {
                ofd.IsFolderPicker = true;

                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    OutputBrowsePath = ofd.FileName;
                }
            }
        }

        private void WorkSpaceBrowseClickCommandMethod()
        {
            using (CommonOpenFileDialog ofd = new CommonOpenFileDialog())
            {
                ofd.IsFolderPicker = true;

                if (ofd.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    WorkSpacePath = ofd.FileName;
                }
            }
        }

        private void ExecuteClickCommandMethod()
        {

            var canRun = VerifyEnvironment();
            
            if (!canRun) return;

            CopyFile();
            QuickLogger.Info("Operation Completed Successfully.");
        }

        private void CopyFile()
        {
            try
            {
                foreach (string file in Directory.GetFiles(WorkSpacePath, "*.py", SearchOption.TopDirectoryOnly))
                {
                    string fileName = Path.GetFileName(Path.GetFileName(file));
                    string directoryLocation = OutputBrowsePath;
                    
                    if (string.IsNullOrEmpty(fileName))
                        return;
                    
                    if (fileName.Contains("_" + UniqueID))
                    {
                        QuickLogger.Info("Res directory path: " + directoryLocation);
                        string path = fileName.Replace("_" + UniqueID, "");
                        string destFileName = Path.Combine(directoryLocation, Path.ChangeExtension(path, "pyp"));
                        File.Copy(file, destFileName, true);
                        QuickLogger.Info("Copied and renamed file " + fileName + " to " + destFileName);
                    }
                }
                DeleteFiles();
            }
            catch (Exception ex)
            {
                QuickLogger.Error(ex.Message);
            }
        }

        private void DeleteFiles()
        {
            try
            {
                foreach (var deletion in DeleteFilesList)
                {
                    if (!string.IsNullOrEmpty(deletion.FilePath))
                    {
                        string fileName = Path.GetFileName(deletion.FilePath);
                        if (DeleteSymbolsCache && fileName.Equals(_symbolsCache) && File.Exists(deletion.FilePath))
                            DeleteFile(deletion.FilePath);
                        else if (DeleteOtherFiles && !fileName.Equals(_symbolsCache))
                            DeleteFile(deletion.FilePath);
                    }
                }

                if (!DeletePYC)
                    return;

                foreach (var directories in PYDirectoryFilesList)
                {
                    if (!string.IsNullOrEmpty(directories.FilePath))
                    {
                        QuickLogger.Info("Preparing to delete PYC Files from " + directories.FilePath + ".");
                        string[] files = Directory.GetFiles(directories.FilePath, "*.pyc", SearchOption.TopDirectoryOnly);
                        QuickLogger.Info($"Deleting {files.Length} PYC Files.");
                        QuickLogger.Info("Preparing to delete PYC Files");
                        foreach (string file in files)
                            DeleteFile(file);
                    }
                }
                QuickLogger.Info("Deletion processes successful!");
            }
            catch (Exception ex)
            {
                QuickLogger.Error("Deletion process unsuccessful:" + Environment.NewLine + ex.Message);
            }
        }

        private void DeleteFile(string file)
        {
            try
            {
                if (string.IsNullOrEmpty(file))
                    return;
                string fileName = Path.GetFileName(file);
                QuickLogger.Info("Attempting to delete file: " + fileName);
                File.Delete(file);
                QuickLogger.Info("Deleted File: " + fileName);
            }
            catch (Exception ex)
            {
                QuickLogger.Error(ex.Message);
            }
        }

        private bool VerifyEnvironment()
        {
            bool passed = true;

            if (string.IsNullOrEmpty(WorkSpacePath))
            {
                QuickLogger.Error("Please check your workspace path.");
                passed = false;
            }

            if (string.IsNullOrEmpty(OutputBrowsePath))
            {
                QuickLogger.Error("Please check your output path.");
                passed = false;
            }

            if (!Directory.Exists(WorkSpacePath))
            {
                QuickLogger.Error("Workspace path doesn't exist.");
                passed = false;
            }

            if (!Directory.Exists(OutputBrowsePath))
            {
                QuickLogger.Error("Output path doesn't exist.");
                passed = false;
            }

            if (!passed)
            {
                QuickLogger.Info("Operation canceled.");
            }
            return passed;
        }

        public async Task SaveData()
        {
            try
            {
                //if (Tab.Header.ToString().StartsWith(DataManager.NewDocumentName))
                //{
                //    MessageBox.Show("This project name has not been set please select the working directory before saving.",
                //        "Working Directory not set!", MessageBoxButton.OK, MessageBoxImage.Stop);
                //    return;
                //}

                bool continueSave = true;

                string newDocumentName = string.Empty;
                if (string.IsNullOrEmpty(SaveLocation))
                {
                    using (CommonSaveFileDialog sfd = new CommonSaveFileDialog())
                    {
                        sfd.AlwaysAppendDefaultExtension = true;
                        sfd.DefaultExtension = "apyp";
                        sfd.DefaultFileName = $"{Tab.GetRawName()}";
                        sfd.Filters.Add(new CommonFileDialogFilter("AutoPYP Project", "*.apyp"));

                        

                        var dialog = sfd.ShowDialog();

                        switch (dialog)
                        {
                            case CommonFileDialogResult.Ok:
                                SaveLocation = sfd.FileName;
                                break;
                            case CommonFileDialogResult.Cancel:
                                continueSave = false;
                                break;
                        }

                    }
                }

                newDocumentName = Path.GetFileNameWithoutExtension(SaveLocation);
                
                if (continueSave)
                {
                    File.WriteAllText(SaveLocation, JsonConvert.SerializeObject(this, Formatting.Indented));

                    if (Tab.IsEdited())
                    {
                        Tab.UpdateLabel(false);
                    }

                    App.MruList.MRUFileList.UpdateEntry(MRU_Service.Create_Entry(SaveLocation, false));

                    Tab.ChangeHeaderName(newDocumentName);

                    await SaveMRUList();
                }
            }
            catch (Exception e)
            {
                QuickLogger.Error(e.Message);
            }
        }

        private async Task SaveMRUList()
        {
            try
            {
                if (!File.Exists(App.MRUSaveLocation))
                {
                    File.Create(App.MRUSaveLocation);
                }

                var t = await MRUEntrySerializer.SaveAsync(App.MRUSaveLocation, App.MruList.MRUFileList);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.StackTrace, exp.Message);
            }
        }

        private void Watch()
        {
            if (WorkSpacePath == null || !DirectoryHelpers.IsDirectory(WorkSpacePath)) return;

            if (_watcher != null)
            {
                UnSubscribe();
                _watcher = null;
            }


            _watcher = new FileSystemWatcher();
            _watcher.Path = WorkSpacePath;
            _watcher.NotifyFilter = NotifyFilters.LastAccess
                                    | NotifyFilters.LastWrite
                                    | NotifyFilters.FileName
                                    | NotifyFilters.Size
                                    | NotifyFilters.DirectoryName;
            _watcher.Filter = "*.*";
            _watcher.Changed += OnChanged;
            _watcher.Created += OnChanged;
            _watcher.Deleted += OnChanged;
            _watcher.Renamed += OnChanged;
            _watcher.EnableRaisingEvents = true;
        }

        public void UnSubscribe()
        {
            if (_watcher == null) return;

            _watcher.Changed -= OnChanged;
            _watcher.Created -= OnChanged;
            _watcher.Deleted -= OnChanged;
            _watcher.Renamed -= OnChanged;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            App.Current.Dispatcher?.Invoke(OnDirectoryUpdate);
        }

        private void PublishClickCommandMethod()
        {
            try
            {
                using (CommonSaveFileDialog sfd = new CommonSaveFileDialog())
                {
                    sfd.DefaultFileName = $"{Tab.GetRawName()}";
                    sfd.DefaultExtension = "zip";
                    sfd.AlwaysAppendDefaultExtension = true;
                    sfd.Filters.Add(new CommonFileDialogFilter("Zip File", "*.zip"));

                    string sourceDirectory = WorkSpacePath;

                    //var g = ;

                    string targetDirectory = Path.Combine(Path.GetTempPath(), Path.GetFileName(WorkSpacePath) ?? throw new InvalidOperationException("Workspace path is null"));

                    if (sfd.ShowDialog() == CommonFileDialogResult.Ok)
                    {
                        try
                        {
                            DirectoryHelpers.Copy(sourceDirectory, targetDirectory);

                            for (int i = DirectoryItemsCollection.Count - 1; i >= 0; i--)
                            {
                                var file = DirectoryItemsCollection[i];
                                if (!file.AllowedToPackage)
                                {
                                    var path = GetModPath(file.FullPath);

                                    if (!string.IsNullOrEmpty(path))
                                    {
                                        DirectoryHelpers.SafeFileDelete(Path.Combine(targetDirectory, path), false);
                                    }
                                }
                            }

                            using (ZipFile zip = new ZipFile())
                            {
                                zip.AddDirectory(targetDirectory, Tab.GetRawName());

                                zip.Save(Path.Combine(WorkSpacePath, sfd.FileName));
                            }
                        }
                        finally
                        {
                            DirectoryHelpers.SafeDeleteDirectory(targetDirectory);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private string GetModPath(string path)
        {
            var array = path.Split(Path.DirectorySeparatorChar).ToList();
            Queue qt = new Queue();

            foreach (string name in array)
            {
                qt.Enqueue(name);
            }

            for (int i = qt.Count - 1; i >= 0; i--)
            {
                if (qt.Peek().ToString() != Tab.GetRawName())
                {
                    qt.Dequeue();
                }
                else
                {
                    qt.Dequeue();
                    array.Clear();
                    foreach (string name in qt)
                    {
                        array.Add(name);
                        return Path.Combine(array.ToArray());
                    }
                    break;
                }
            }
            return string.Empty;
        }
    }
}

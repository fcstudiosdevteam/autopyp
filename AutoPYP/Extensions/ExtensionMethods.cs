﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace AutoPYP.Extensions
{
    public static class ExtensionMethods
    {
        public static string OffVariant(this string path)
        {
            return Path.ChangeExtension(path, "off");
        }
    }
}

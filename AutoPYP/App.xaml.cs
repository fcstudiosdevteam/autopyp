﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using AutoPYP.Models;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace AutoPYP
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Window _window;

        public static string Version { get; set; }
        public static bool Initialized { get; set; }
        public static MRUList MruList { get; set; }
        public static string MRUSaveLocation => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "System", "MRU","MRULIST.xml");

        public static void StayOnTopCommandMethod(bool value, Window window = null)
        {
            if(window != null)
                _window = window;

            if (_window == null)
            {
                MessageBox.Show("AutoPYP Internal Error: Cannot find Window", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                _window.Topmost = value;
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Version = $"Version: {Assembly.GetExecutingAssembly().GetName().Version}";

            MruList = new MRUList();
        }
    }
}

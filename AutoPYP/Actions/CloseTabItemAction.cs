﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using AutoPYP.Models;
using AutoPYP.Models.UIElements;

namespace AutoPYP.Actions
{
    public class CloseTabItemAction : TriggerAction<DependencyObject>
    {
        protected override void Invoke(object parameter)
        {
            DataManager.CloseTab(TabItem);
        }

        public static readonly DependencyProperty TabControlProperty =
            DependencyProperty.Register("TabControl", typeof(TabControl), typeof(CloseTabItemAction), new PropertyMetadata(default(TabControl)));

        public TabControl TabControl
        {
            get => (TabControl)GetValue(TabControlProperty);
            set => SetValue(TabControlProperty, value);
        }

        public static readonly DependencyProperty TabItemProperty =
            DependencyProperty.Register("TabItem", typeof(TabItem), typeof(CloseTabItemAction), new PropertyMetadata(default(TabItem)));

        public CustomTab TabItem
        {
            get => (CustomTab)GetValue(TabItemProperty);
            set => SetValue(TabItemProperty, value);
        }
    }
}